<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\ProductRepository;
use App\Services\MarketService;
use Illuminate\Http\Request;
use App\Entities\Product;
use App\User;

class MarketServiceTest extends TestCase
{
    protected $repositoryMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repositoryMock = $this->createMock(ProductRepository::class);


    }

    public function test_get_all_product()
    {
        $this->repositoryMock->method('findAll')
            ->will($this->returnCallback(function ()
            {
                return new Collection(self::productsData());
            }));

        $marketServise = new MarketService($this->repositoryMock);

        $products = $marketServise->getProductList();

        $this->assertInstanceOf(Collection::class, $products);
        $this->assertNotEmpty($products);
        $this->assertContainsOnlyInstancesOf(Product::class, $products);
        $this->assertCount(3, $products);
    }

    public function test_get_product_by_id()
    {
        $this->repositoryMock->method('findById')
            ->will($this->returnCallback(function ()
            {
                return self::getProductById(3);
            }));

        $marketServise = new MarketService($this->repositoryMock);

        $product = $marketServise->getProductById(3);

        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals(3, $product->id);
        $this->assertEquals('3 product', $product->name);
        $this->assertEquals(300, $product->price);
        $this->assertEquals('3', $product->user_id);
    }

    public function test_can_not_get_product_by_id()
    {
        $this->repositoryMock->method('findById')
            ->will($this->returnCallback(function ()
            {
                return self::getProductById(4);
            }));

        $marketServise = new MarketService($this->repositoryMock);

        try {
            $marketServise->getProductById(4);
        } catch (\Exception $e) {
            $this->assertStringContainsString('No product with id: 4', $e);
        }

    }

    public function test_get_products_by_user_id()
    {
        $this->repositoryMock->method('findByUserId')
            ->will($this->returnCallback(function ()
            {
                return new Collection(self::getProductsByUserId(1));
            }));

        $marketServise = new MarketService($this->repositoryMock);

        $products = $marketServise->getProductsByUserId(1);

        $this->assertNotEmpty($products);
        $this->assertContainsOnlyInstancesOf(Product::class, $products);
        $this->assertInstanceOf(Collection::class, $products);
        $this->assertEquals(1, $products->first()->user_id);
        $this->assertCount(1, $products);
    }

    public function test_get_products_by_non_existent_user_id()
    {
        $this->repositoryMock->method('findByUserId')
            ->will($this->returnCallback(function ()
            {
                return new Collection(self::getProductsByUserId(99));
            }));

        $marketServise = new MarketService($this->repositoryMock);

        $products = $marketServise->getProductsByUserId(99);

        $this->assertEmpty($products);
        $this->assertInstanceOf(Collection::class, $products);
        $this->assertCount(0, $products);
    }

    public function test_create_product()
    {
        $user = factory(User::class)->make(['id' => 4]);
        $this->actingAs($user);

        $this->repositoryMock->method('store')
            ->will($this->returnCallback(function () use($user)
            {
                return new Product([
                    'id' => 4,
                    'user_id' => auth()->id(),
                    'name' => 'Test Product',
                    'price'    => 400
                ]);
            }));

        $request = $this->createMock(Request::class);

        $requestParams = [
            'product_name' => 'Test Product',
            'product_price'    => 400
        ];

        $request->expects($this->exactly(2))
            ->method('input')
            ->willReturnMap($requestParams);

        $marketServise = new MarketService($this->repositoryMock);

        $product = $marketServise->storeProduct($request);

        $this->assertNotEmpty($product);
        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals(4, $product->user_id);
        $this->assertEquals('Test Product', $product->name);
        $this->assertEquals(400, $product->price);
        $this->assertEquals(4, $product->id);
    }

    private static function getProductById(int $id): ?Product
    {
        foreach (self::productsData() as $product) {
            if ($id === $product->id) {

                return $product;
            }
        }

        return null;
    }

    private static function getProductsByUserId(int $id): ?array
    {
        foreach (self::productsData() as $product) {
            if ($id === $product->user_id) {
                $products[] = $product;
            }
        }

        if (!empty($products)) {

            return $products;
        } else {

            return null;
        }
    }

    private static function productsData(): array
    {
        return [
            new Product([
                'id' => 1,
                'name' => '1 product',
                'price' => 100,
                'user_id' => 1,
            ]),
            new Product([
                'id' => 2,
                'name' => '2 product',
                'price' => 200,
                'user_id' => 2,
            ]),
            new Product([
                'id' => 3,
                'name' => '3 product',
                'price' => 300,
                'user_id' => 3,
            ]),
        ];
    }

}
