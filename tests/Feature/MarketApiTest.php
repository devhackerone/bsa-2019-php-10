<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\User;
use App\Entities\Product;

class MarketApiTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        factory(User::class, 3)
            ->create()
            ->each(function ($user) {
                $user->products()->saveMany(factory(Product::class, 10)->make()
                );
            });
    }

    public function test_show_list_product()
    {
        $products = $this->json('get', 'api/items')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'price',
                    'user_id'
                ]
            ])
            ->assertJsonCount(30)
            ->json();

        $this->assertNotEmpty($products);
        $this->assertCount(30, $products);

        foreach ($products as $key) {
            $this->assertArrayHasKey('id', $key);
            $this->assertArrayHasKey('name', $key);
            $this->assertArrayHasKey('price', $key);
            $this->assertArrayHasKey('user_id', $key);
        }
    }

    public function test_show_product()
    {
       $products = $this->json('get', "api/items/3")
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'name',
                'price',
                'user_id',
            ]);

        $this->assertNotEmpty($products);
    }

    public function test_fail_show_product()
    {
        Product::destroy(500);

        $products = $this->json('get', 'api/items/500')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(400)
            ->assertJsonStructure([
                'message',
            ])
            ->json();

        $this->assertEquals('fail', $products['message']);
    }

    public function test_store_product()
    {
        $user = User::inRandomOrder()->first();
        $this->actingAs($user, 'api');


        $this->json('post', 'api/items', [
            'product_name' => 'Test product',
            'product_price' => 500,
        ])
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(201)
            ->assertExactJson([
            'id' => 31,
            'name' => 'Test product',
            'price' => 500,
            'user_id' => $user->id,
            ]);

        $this->assertDatabaseHas('products', [
            'id' => 31,
            'name' => 'Test product',
            'price' => 500,
            'user_id' => $user->id,
        ]);

    }

    public function test_guest_can_not_create_product()
    {
        $this->json('post', 'api/items', [
            'name' => 'Test product',
            'price' => 500,
        ])
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(401)
            ->assertJsonFragment([
                'message' => 'Unauthenticated.',
            ]);

    }

    public function test_user_can_not_create_invalid_product()
    {
        $user = User::inRandomOrder()->first();
        $this->actingAs($user, 'api');


        $this->json('post', 'api/items', [
            'product_name' => 998,
            'product_price' => 'price 500',
        ])
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(400)
            ->assertJsonFragment([
                'message' => 'fail',
            ])
            ->assertJsonValidationErrors([
                'product_name',
                'product_price',
            ]);
    }

    public function test_delete_user_product()
    {
        $user = User::inRandomOrder()->first();
        $this->actingAs($user, 'api');

        $product = factory(Product::class)->create(['user_id' => $user->id]);

        $this->json('delete', "api/items/{$product->id}")
            ->assertStatus(204);

        $this->assertDatabaseMissing('products', ['id' => $product->id]);
    }

    public function test_fail_delete_product()
    {
        $user_id = User::inRandomOrder()->first();
        $this->actingAs($user_id, 'api');

        Product::destroy(500);

        $products = $this->json('delete', 'api/items/500')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(400)->json();

        $this->assertEquals('fail', $products['message']);
    }

    public function test_forbidden_delete_product()
    {
        $user_id = User::inRandomOrder()->first();
        $this->actingAs($user_id, 'api');

        $product = Product::whereNotIn('user_id', $user_id)->first();

        $data = $this->json('delete', "api/items/{$product->id}")
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ])
            ->json();

        $this->assertEquals('Forbidden', $data['message']);
    }

}
